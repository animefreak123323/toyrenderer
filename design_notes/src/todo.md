* Switch over to glTF
* Remake the mesh system to support
nested meshes.
* Rework the shader system according to the design doc.
